import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;

public class Test {

    private CuratorFramework build;

    @Before
    public void testBefore() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(300, 10);
        build = CuratorFrameworkFactory.builder().connectString("192.168.9.131:2181").namespace("ayq").retryPolicy(retryPolicy).build();
        build.start();
    }


    @org.junit.Test
    public void test1() throws Exception {
        String s = build.create().withMode(CreateMode.PERSISTENT).forPath("/app5");
        System.out.println(s);
    }

    @org.junit.Test
    public void test2() throws Exception {
        Stat stat = build.setData().forPath("/ayq1", "hehe1".getBytes());
        System.out.println(stat);
    }

    @org.junit.Test
    public void test3() throws Exception {
        build.delete().guaranteed().inBackground(new BackgroundCallback() {
            public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
                System.out.println(curatorEvent);
                System.out.println("删除了");
            }
        }).forPath("/app5");
    }

    @org.junit.Test
    public void test4() throws Exception {
        final NodeCache nodeCache = new NodeCache(build, "/ayq1");
        nodeCache.getListenable().addListener(new NodeCacheListener() {
            public void nodeChanged() throws Exception {
                System.out.println("节点改变了");
                byte[] data = nodeCache.getCurrentData().getData();
                System.out.println(new String(data));
            }
        });
        nodeCache.start(true);

        while (true) {

        }
    }

    @After
    public void testAfter() {
        if (build != null) {
            build.close();
        }
    }
}
