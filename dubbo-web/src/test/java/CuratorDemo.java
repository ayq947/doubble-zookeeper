import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/*@RunWith(SpringJUnit4ClassRunner.class)
@Configuration("classpath:spring/springmvc.xml")*/
public class CuratorDemo {

    private  CuratorFramework clent;

    @Before
    public void test(){
        RetryPolicy retryPolicy =  new ExponentialBackoffRetry(3000,10);
        //第一种构建
        /*CuratorFramework clent = CuratorFrameworkFactory.newClient("192.168.9.131:2181",
                60 * 1000, 15 * 1000, retryPolicy);
        curatorFramework.start();*/
        //第二种构建
        clent = CuratorFrameworkFactory.builder().connectString("192.168.9.131:2181").connectionTimeoutMs(15 * 1000)
                .sessionTimeoutMs(60 * 1000).retryPolicy(retryPolicy).namespace("ayq").build();
        clent.start();

    }

    @Test
    public void createNode(){
        try {
            clent.create().forPath("/ayq1");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createNode1(){
        try {
            clent.create().withMode(CreateMode.EPHEMERAL).forPath("/ayq2","1234".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createNode2(){
        try {
            clent.create().creatingParentsIfNeeded().forPath("/ayq2","1234".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void get(){
        try {
            byte[] bytes = clent.getData().forPath("/ayq2");
            System.out.println(new String(bytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void get1(){
        try {
            List<String> list = clent.getChildren().forPath("/");
            for (String s : list) {
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void get2(){
        try {
            Stat stat = new Stat();
            System.out.println(stat);
            byte[] bytes = clent.getData().storingStatIn(stat).forPath("/ayq2");
            System.out.println(new String(bytes));
            System.out.println(stat);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void set2(){
        try {
            Stat stat = new Stat();
            System.out.println(stat);
            byte[] bytes = clent.getData().storingStatIn(stat).forPath("/ayq2");
            System.out.println(new String(bytes));
            System.out.println(stat);
            clent.setData().withVersion(stat.getVersion()).forPath("/ayq2");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public  void close(){
        clent.close();
    }
}
