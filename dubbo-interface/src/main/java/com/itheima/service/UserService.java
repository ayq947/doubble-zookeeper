package com.itheima.service;

import com.test.UserBean;

public interface UserService {


    public String sayHello();

    public UserBean getUserBeanById(int id);
}
